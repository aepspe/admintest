﻿using MyClothBazwe.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyClothBazar.Database
{
    public class CBContaxt : DbContext
    {
        public CBContaxt() : base("MyClothBazerConnection")
        {

        }
        public DbSet<Cotegory> Categories { get; set; }
        public DbSet<Product> Products { get; set; }
    }
}
