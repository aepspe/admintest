namespace MyClothBazar.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initialized : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cotegories",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Discription = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Name = c.String(),
                        Discription = c.String(),
                        Cotegory_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Cotegories", t => t.Cotegory_ID)
                .Index(t => t.Cotegory_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Products", "Cotegory_ID", "dbo.Cotegories");
            DropIndex("dbo.Products", new[] { "Cotegory_ID" });
            DropTable("dbo.Products");
            DropTable("dbo.Cotegories");
        }
    }
}
