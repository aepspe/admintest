﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MyClothBazer.Web.Startup))]
namespace MyClothBazer.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
